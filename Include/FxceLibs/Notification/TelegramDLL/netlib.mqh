//+------------------------------------------------------------------+
//|                                                  NetLib.mqh |
//|                                 Copyright © 2021 www.fxce.com |
//|                                         Coding by ThanhDong |
//+------------------------------------------------------------------+
#property copyright   "www.fxce.com  © 2021"
#property link        "www.fxce.com"
#property version     "1.00"
#property description "Library for work with wininet.dll"
#property library

#import  "Wininet.dll"
	int InternetAttemptConnect(int x);
   int InternetOpenW(string, int, string, string, int);
   int InternetConnectW(int, string, int, string, string, int, int, int); 
   int InternetOpenUrlW(int, string, string, int, int, int);
   int InternetReadFile(int, string, int, int& OneInt[]);
   int InternetCloseHandle(int); 
   int HttpOpenRequestW(int, string, string, string, string,  string, int, int);
   bool HttpSendRequestW(int, string, int, char &rq[], int);
   int InternetReadFile(int hFile,uchar &sBuffer[],int lNumBytesToRead,int &lNumberOfBytesRead);
#import
#import "kernel32.dll"
int GetLastError(void);
#import

#define HTTP_VERB_GET "GET"
#define HTTP_VERB_POST "POST"
#define HTTP_VERB_PUT "PUT"
#define INTERNET_FLAG_SECURE 0x00800000
#define HTTP_QUERY_CONTENT_LENGTH 5
#define HTTP_QUERY_STATUS_CODE 19
#define HTTP_QUERY_FLAG_NUMBER 0x20000000
#define INTERNET_FLAG_NO_AUTO_REDIRECT 0x00200000	// no auto redirect
#define INTERNET_FLAG_PRAGMA_NOCACHE   0x00000100  // no caching of page
#define INTERNET_FLAG_KEEP_CONNECTION  0x00400000  // keep connection
#define INTERNET_FLAG_RELOAD           0x80000000  // get page from server when calling it

class MqlNet
{
   string            Host;       // host name
   int               Port;       // port
   int               Session;    // session descriptor
   int               Connect;    // connection descriptor
	
	public:
                     MqlNet();   // class constructor
                    ~MqlNet();   // destructor
   bool              Open(string aHost,int aPort); 						// create session and open connection
   void              Close();    												// close session and connection
   bool              Request(string Header,string Method,string Params, string &Response, string requestData, bool toFile, string fileName); 	// send request
   bool 					Request2(string Header, string Method,string Params,string &Response,char &data[], bool toFile=false, string fileName="");
   bool              OpenURL(string URL,string &Out,bool toFile, string fileName); 	// open page
   void              ReadPage(int hRequest,string &Out,bool toFile,string fileName); // read page
   long              GetContentSize(int hURL); 								//get content size
   int               FileToArray(string FileName,uchar &data[]); 		// copying file to the array
};
//------------------------------------------------------------------ MqlNet
void MqlNet::MqlNet()
{
	Session=-1;
	Connect=-1;
	Host="";
}
//------------------------------------------------------------------ ~MqlNet
void MqlNet::~MqlNet()
{
	Close();
}

//+------------------------------------------------------------------+
//|   @brief open connect to server.  
//|   @param inHost hosting to connect.
//|   @param inPort port to connect.
//|	@return Result conection success of fail
//+------------------------------------------------------------------+
bool MqlNet::Open(string inHost,int inPort)
{
	if(inHost=="")
	{
		Print("Host is not specified");
		return(false);
  	}
   
  	if(Session>0 || Connect>0) Close();
   	Print("Open Inet...");

	if(InternetAttemptConnect(0)!=0)
	{
		Print("Err AttemptConnect");
		return(false);
	}
   
   string UserAgent="";
   Session=InternetOpenW(UserAgent,0," ","",0);
   // exit, if session is not opened
   if(Session<=0)
	{
		Print("Error create Session");
		Close();
		return(false);
	}
   Connect=InternetConnectW(Session,inHost,inPort,"","",3,0,0);
   if(Connect<=0)
	{
		Print("Error create Connect");
		Close();
		return(false);
	}
   Host=inHost; 
   Port=inPort;
   return(true);
}

//+------------------------------------------------------------------+
//|   @brief close connection  
//+------------------------------------------------------------------+
void MqlNet::Close()
{
	if(Session>0) InternetCloseHandle(Session);
	Session=-1;
	if(Connect>0) InternetCloseHandle(Connect);
	Connect=-1;
}

//+------------------------------------------------------------------+
//|   @brief send request to server.  
//|   @param Header is data header of request.
//|   @param Method method request POST - GET - PUT.
//|   @param Parame parameter to send request.
//|   @param Response to receive result of reponse from server.
//|   @param RequestData data body request to server.
//|   @param toFile have write response to file.
//|   @param fileName nam of file to write.
//|	@return Result conection success of fail
//+------------------------------------------------------------------+
bool MqlNet::Request(string Header, string Method,string Params,string &Response,string RequestData="", bool toFile=false, string fileName="")
{   
   char data[];
   string Version="HTTP/1.1";
   string acceptTypes="";
	StringToCharArray(RequestData,data,0,StringLen(RequestData));
	
   if(Session<=0 || Connect<=0)
	{
		Close();
		if(!Open(Host,Port))
		{
		   Print("Error Connect");
		   Close();
		   return(false);
		}
	}
  
   int httpRequest=HttpOpenRequestW(Connect,Method,Params,Version,"",acceptTypes,INTERNET_FLAG_KEEP_CONNECTION|INTERNET_FLAG_RELOAD|INTERNET_FLAG_PRAGMA_NOCACHE|INTERNET_FLAG_SECURE|INTERNET_FLAG_NO_AUTO_REDIRECT, 0);
   if(httpRequest<=0)
  	{
	   Print("Error OpenRequest");
	   InternetCloseHandle(Connect);
	   return(false);
   }
   
   int httpSend=HttpSendRequestW(httpRequest,Header,StringLen(Header),data,ArraySize(data));
   if(httpSend<=0)
   {
	   Print("Error SendRequest");
	   InternetCloseHandle(httpRequest);
	   Close();
   }
   ReadPage(httpRequest,Response,toFile,fileName);
   InternetCloseHandle(httpRequest);
   InternetCloseHandle(httpSend);
   return(true);
}

bool MqlNet::Request2(string Header, string Method,string Params,string &Response,char &data[], bool toFile=false, string fileName="")
{   
   //char data[];
   string Version="HTTP/1.1";
   string acceptTypes="";
	//StringToCharArray(RequestData,data,0,StringLen(RequestData));
	
   if(Session<=0 || Connect<=0)
	{
		Close();
		if(!Open(Host,Port))
		{
		   Print("Error Connect");
		   Close();
		   return(false);
		}
	}
  
   int httpRequest=HttpOpenRequestW(Connect,Method,Params,Version,"",acceptTypes,INTERNET_FLAG_KEEP_CONNECTION|INTERNET_FLAG_RELOAD|INTERNET_FLAG_PRAGMA_NOCACHE|INTERNET_FLAG_SECURE|INTERNET_FLAG_NO_AUTO_REDIRECT, 0);
   if(httpRequest<=0)
  	{
	   Print("Error OpenRequest");
	   InternetCloseHandle(Connect);
	   return(false);
   }
   
   int httpSend=HttpSendRequestW(httpRequest,Header,StringLen(Header),data,ArraySize(data));
   if(httpSend<=0)
   {
	   Print("Error SendRequest");
	   InternetCloseHandle(httpRequest);
	   Close();
   }
   ReadPage(httpRequest,Response,toFile,fileName);
   InternetCloseHandle(httpRequest);
   InternetCloseHandle(httpSend);
   return(true);
}

//+------------------------------------------------------------------+
//|   @brief readpage data response.  
//|   @param httpRequest is variable request to server.
//|   @param Response contain data response.
//|   @param toFile have write response to file.
//|   @param fileName nam of file to write.
//+------------------------------------------------------------------+
void MqlNet::ReadPage(int httpRequest,string &Response,bool toFile, string fileName)
{
	uchar ch[100];
	string toStr="";
	int dwBytes,h;
	while(InternetReadFile(httpRequest,ch,100,dwBytes))
	  {
	   if(dwBytes<=0) break;
	   toStr=toStr+CharArrayToString(ch,0,dwBytes);
	  }
	if(toFile)
	  {
	   h=FileOpen(fileName,FILE_BIN|FILE_WRITE);
	   FileWriteString(h,toStr);
	   FileClose(h);
	  }
	Response=toStr;
}

//+------------------------------------------------------------------+
//|   @brief open connect to url to get data.  
//|   @param URL to get data from server.
//|   @param Out contain data response.
//|   @param toFile have write response to file.
//|   @param fileName nam of file to write.
//+------------------------------------------------------------------+
bool MqlNet::OpenURL(string URL,string &Out, bool toFile, string fileName)
{
	string nill="";
	if(Session<=0 || Connect<=0)
	{
		Close();
		if(!Open(Host,Port))
	  	{
		   Print("Error Connect");
		   Close();
		   return(false);
	  	}
	}
	int hURL=InternetOpenUrlW(Session, URL, nill, 0, INTERNET_FLAG_RELOAD|INTERNET_FLAG_PRAGMA_NOCACHE, 0);
	if(hURL<=0)
	{
		Print("Error OpenUrl");
		return(false);
	}
	// read to Out
	ReadPage(hURL,Out,toFile,fileName);
	InternetCloseHandle(hURL);
	return(true);
}

//+------------------------------------------------------------------+
//|   @brief read data from file.  
//|   @param FileName file contain data.
//|   @param data contain data read from file.
//+------------------------------------------------------------------+
int MqlNet::FileToArray(string FileName,uchar &data[])
{
	int h,i,size;
	h=FileOpen(FileName,FILE_BIN|FILE_READ);
	if(h<0) return(-1);
	FileSeek(h,0,SEEK_SET);
	size=(int)FileSize(h);
	ArrayResize(data,(int)size);
	for(i=0; i<size; i++)
	  {
	   data[i]=(uchar)FileReadInteger(h,CHAR_VALUE);
	  }
	FileClose(h); return(size);
}