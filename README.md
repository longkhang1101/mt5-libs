# Welcome to FXCE Library

Find anything you want in a library with FXCE, a **largest global catalog** of trading code collections. The library for traders and coders is created **non-profit**, so we do not charge any fees involved. Whenever you are stuck with your trading bots or ideas, **the best place to start** is our library and we enable you. 

## What You May Know

***FXCE builds the whole new era of trading for you to lead the industry with our technology. Being the game changer has never been this simple without us.***

FXCE has always kept helping the community use technology to optimize their trade as our priority. For that reason, we have laid the foundations for capital, technology and expertise. Any traders can develop their own Expert Advisor (EA) via our **Training, Development and Evaluation Program**.

Within the framework of the program, we build an exclusive **FXCE EA library system**. This library also serves the purpose that the communities in our ecosystem can access this resource in the long term.

## Currently We Provide

The FXCE Library improves community’s **quality of automatic trade** by supporting their efforts to develop their own EAs or use our available ones together with knowledge and idea sharing or EA testing. Until now, we delightedly introduced these following **dedicated libraries**.  

### FXCE - MQL5 Open Source Library

Your ability to modify and create the new EA from original source codes proves this is absolutely for you. Not having to write each single line of code, a number of professional codes but clean and easy to modify are approachable. 

==========================

Please click [**here**](https://gitlab.com/fxce/mt5-libs/-/wikis/FXCE---MQL5-Open-Source-Library) for more infomation.

==========================

### FXCE Expert Advisor Library

Do you want something instant but still professional and effective for your automatic trading system? Just list feature requirements, check it out right here and you will find what you need among our complete trading robots. By using our EA, please acknowledge our risk disclaimer. 

==========================

Please click [**here**](https://gitlab.com/fxce/mt5-libs/-/wikis/FXCE-Expert-Advisor-Library) for more infomation.

==========================


### FXCE Indicator Library

No matter what your style is and how you plan your trade, technical analysis cannot lack its indicators. Our MQL5 indicators can help analyze price charts, predict the behavior of the market as well as make trading decisions. Visit our collection of the best and expert MT5 indicators for free. 

==========================

Please click [**here**](https://gitlab.com/fxce/mt5-libs/-/wikis/FXCE-Indicators-Library) for more infomation.

==========================

